# Rickrollify

An extension that grants you a 1% chance of injecting Rick Astley on every new page load.

## Chrome

Go to chrome://extensions and enable Developer mode (toggle in top right).
Download the .crx from [Releases](https://codeberg.org/duck/Rickrollify/releases) and drag-and-drop it onto the chrome://extensions page.

## Firefox

[Download](https://addons.mozilla.org/en-US/firefox/addon/rickrollify/)
